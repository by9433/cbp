# CBP

Connectivity-based parcellation using fMRI time series

* **Please cite the paper if you use this code** \
*B.-y. Park, K.-J. Tark, W. M. Shim, and H. Park.* Functional connectivity based parcellation of early visual cortices. *Human Brain Mapping* 39(3):2
90-1390(2018). \
http://onlinelibrary.wiley.com/doi/10.1002/hbm.23926/full

# Prerequisite

* **MATLAB >= v.2016b** 
* Please download the *matlab toolbox for dimensionality reduction* and add to your path \
https://lvdmaaten.github.io/drtoolbox/ 

# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/
* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl