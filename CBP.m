clear;clc; close all;

% Load time series
ts = importdata('ts.txt');    % size: [number of time points x number of ROIs]
Nts = size(ts,1);    % number of time points
Nroi = size(ts,2);  % number of ROIs

% Normalize the time series (demean and standardize)
ts_norm = ts - repmat(mean(ts),size(ts,1),1);
ts_norm = ts_norm/std(ts_norm(:));

% Construct connectivity matrix
CorrMat = corr(ts_norm);
CorrMat(eye(Nroi)>0)=0;
CorrMatZ = .5.*log((1+CorrMat)./(1-CorrMat));     % Fisher's r-to-z transform     R =.5.*log((1+R)./(1-R));

% Reduce the dimensionality of the matrix
num_dims = floor(intrinsic_dim(CorrMatZ', 'EigValue'));     % estimate the dimension
CorrMatZ_DimRed = compute_mapping(CorrMatZ', 'PCA', num_dims);      % apply PCA

% Determine the number of clusters using silhouette coefficients
total_silhouette = zeros(3,10);
% 3: different clustering methods (GMMdist, kmeans, ward)
% 10: number of clusters
for nc = 2:10
    % GMMdist
    obj = gmdistribution.fit(CorrMatZ_DimRed, nc);
    idx = cluster(obj, CorrMatZ_DimRed);
    s = silhouette(CorrMatZ_DimRed, idx);
    total_silhouette(1,nc) = mean(s);
    % kmeans
    idx = kmeans(CorrMatZ_DimRed,nc);
    s = silhouette(CorrMatZ_DimRed, idx);
    total_silhouette(2,nc) = mean(s);
    % ward
    idx = clusterdata(CorrMatZ_DimRed, 'linkage', 'ward', 'distance', 'euclidean', 'maxclust', nc); % ward: use Euclidean distance
    s = silhouette(CorrMatZ_DimRed, idx);
    total_silhouette(3,nc) = mean(s);
end

% Plot the silhouette coefficients
figure; plot(total_silhouette(1,:), 'r:^', 'LineWidth', 2);
hold on;
plot(total_silhouette(2,:), 'g:o', 'LineWidth', 2);
plot(total_silhouette(3,:), 'b:*', 'LineWidth', 2);
legend('GMM', 'K-means','Ward');
hold off;
axis([1 10 0 0.7]);
set(gca,'XTick',[0:1:10]);
set(gca,'YTick',[0:0.1:1]);
set(gca,'FontSize',12);
xlabel('Number of clusters', 'fontsize', 15, 'fontweight', 'bold');
ylabel('Silhouette coefficient', 'fontsize', 15, 'fontweight', 'bold');

% Perform clustering witih three different clustering methods
NumCluster = 2;
% GMMdist (Gaussian mixture model distribution clustering)
obj = gmdistribution.fit(CorrMatZ_DimRed, NumCluster);
idx_gmm = cluster(obj, CorrMatZ_DimRed);
% K-means clustering
idx_kmeans = kmeans(CorrMatZ_DimRed,NumCluster);
% Ward's hierarchical clustering
idx_ward = clusterdata(CorrMatZ_DimRed, 'linkage', 'ward', 'distance', 'euclidean', 'maxclust', NumCluster);
